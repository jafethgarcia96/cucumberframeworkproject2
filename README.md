The "CucumberFrameWorkProject2" uses a POM model, it is achieved in the framework by 
creating various folders or Packages such as "runners", "pages", "steps", "utilities", 
and our "features" folder in the "resources" folder. All these folders are in the test java folder. 
All these folders and classes within them make it easier to achieve code reusilability.
Our utitlities has classes with various methods such as a driver encapsulation method and all our methods 
can be applied anywhere in the project, but usually in our steps classes where all of our code test cases is actually implemented.
And we use our pages to store our web elements, which are also used in the steps classes. When our step classes/class is done We can run everything or specific cases in our runner class by specifying with the tags we gave our test cases. All the mentioned steps are read by our feature file, which is where we write our cases in gherkin format. 
Finally in our pom.xml file which comes from maven we define all our dependencies we will use for our BDD framework, in this case i used the following;
WebDriverManager- to make it easier to set up our driver
Selenium WebDriver- to automate ui web based apps
HtmlUnitDriver- for headless testing
JUnit- for its annotations and assertions
Cucumber- to achieve a BDD framework and use feature files
maven-surefire - to implement more testing reports or commands 
