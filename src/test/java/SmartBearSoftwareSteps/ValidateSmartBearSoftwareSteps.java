package SmartBearSoftwareSteps;

import SmartBearSoftwarePages.*;
import com.github.javafaker.Faker;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java.eo.Se;
import io.cucumber.datatable.DataTable;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import utilities.Driver;
import utilities.Waiter;

import java.util.List;

public class ValidateSmartBearSoftwareSteps {
    public WebDriver driver;
    LoginPage loginPage;
    viewAllOrdersPage viewAllOrdersPage;
    OrderPage orderPage;
    Select select;
    Faker faker;
    @Before
    public void setUp(){
        driver = Driver.getDriver();
        loginPage = new LoginPage(driver);
        viewAllOrdersPage = new viewAllOrdersPage(driver);
        orderPage = new OrderPage(driver);
        faker = new Faker();
    }

    @Then("user should be routed to {string}")
    public void userShouldBeRoutedTo(String url) {
        Assert.assertEquals(driver.getCurrentUrl(),url);
    }

    @And("validate below menu items are displayed")
    public void validateBelowMenuItemsAreDisplayed(DataTable table) {
        List<String> menuItems = table.asList();
        for (int i = 0; i < menuItems.size(); i++) {
            Assert.assertTrue(viewAllOrdersPage.menuItems.get(i).isDisplayed());
        }
    }

    @When("user clicks on {string} button")
    public void user_clicks_on_button(String string) {
        switch (string){
            case "Check All":
                viewAllOrdersPage.checkUncheck.get(0).click();
                break;
            case "Uncheck All":
                viewAllOrdersPage.checkUncheck.get(1).click();
                break;
            case "Login":
                loginPage.loginButton.click();
                break;
            case "Order":
                viewAllOrdersPage.menuItems.get(2).click();
                break;
            case "Process":
                orderPage.processBttn.click();
                break;
            case "View all orders":
                viewAllOrdersPage.menuItems.get(0).click();
                break;
            case "Delete Selected":
                viewAllOrdersPage.deleteSelectedBttn.click();
                break;
        }
    }

    @Then("all rows should be checked")
    public void all_rows_should_be_checked() {
        for (int i = 0; i < viewAllOrdersPage.checkBoxes.size(); i++) {
            Assert.assertTrue(viewAllOrdersPage.checkBoxes.get(i).isSelected());
        }
    }

    @When("user clicks on “Uncheck All” button")
    public void user_clicks_on_Uncheck_All_button() {
        viewAllOrdersPage.checkUncheck.get(1).click();
    }

    @Then("all rows should be unchecked")
    public void all_rows_should_be_unchecked() {
        for (int i = 0; i < viewAllOrdersPage.checkBoxes.size(); i++) {
            Assert.assertFalse(viewAllOrdersPage.checkBoxes.get(i).isSelected());
        }
    }

    @When("user selects {string} as product")
    public void user_selects_as_product(String string) {
        select = new Select(orderPage.select);
        select.selectByValue(string);
    }

    @When("user enters {int} as quantity")
    public void user_enters_as_quantity(Integer quantity) {
        orderPage.productInfoInputs.get(0).sendKeys(String.valueOf(quantity));
    }

    @When("user enters all address information")
    public void user_enters_all_address_information() {

        for (int i = 0; i < orderPage.addressInfoInputs.size(); i++) {
            orderPage.addressInfoInputs.get(i).sendKeys(Texts.contactInfo[i]);
        }
    }

    @When("user enters all payment information")
    public void user_enters_all_payment_information() {
        orderPage.paymentRadioBttns.get(0).click();

        for (int i = 0; i < orderPage.paymentInputs.size(); i++) {
            orderPage.paymentInputs.get(i).sendKeys(Texts.cardInfo[i]);
        }
    }

    @Then("user should see their order displayed in the {string} table")
    public void user_should_see_their_order_displayed_in_the_table(String string) {
        for (int i = 0; i < viewAllOrdersPage.firstRowTd.size(); i++) {
            Assert.assertTrue(viewAllOrdersPage.firstRowTd.get(i).isDisplayed());
        }
    }

    @Then("validate all information entered displayed correct with the order")
    public void validate_all_information_entered_displayed_correct_with_the_order() {
        int count=0;
        for (int i = 1; i < viewAllOrdersPage.firstRowTd.size()-1; i++) {
            Assert.assertEquals( Texts.firstRowData[count++] , viewAllOrdersPage.firstRowTd.get(i).getText() );
        }
    }

    @Then("validate all orders are deleted from the {string}")
    public void validateAllOrdersAreDeletedFromThe(String orders) {
        for (WebElement row: viewAllOrdersPage.checkBoxes) {
            Assert.assertFalse(row.isDisplayed());
        }
        
        
    }

    @And("validate user sees {string} Message")
    public void validateUserSeesMessage(String message) {
    Assert.assertEquals(message, viewAllOrdersPage.listIsEmptyMssg.getText());
    }
}
