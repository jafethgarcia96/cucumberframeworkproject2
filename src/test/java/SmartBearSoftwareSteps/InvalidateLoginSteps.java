package SmartBearSoftwareSteps;

import SmartBearSoftwarePages.LoginPage;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import utilities.Driver;

public class InvalidateLoginSteps {
    public WebDriver driver;
    LoginPage loginPage;

    @Before
    public void setUp(){
       driver = Driver.getDriver();
       loginPage = new LoginPage(driver);
    }

    @Given("user is on {string}")
    public void user_is_on(String string) {
        driver.get(string);
    }

    @When("user enters username as {string}")
    public void user_enters_username_as(String string) {
        switch (string){
            case "Tester":
            case "abcd":
                loginPage.userAndPasswordInputs.get(0).sendKeys(string);
                break;
        }

    }

    @When("user enters password as {string}")
    public void user_enters_password_as(String string) {
        if (string.equals("test")) loginPage.userAndPasswordInputs.get(1).sendKeys(string);
        else loginPage.userAndPasswordInputs.get(1).sendKeys(string);
    }

    /*@When("user clicks on Login button")
    public void user_clicks_on_Login_button() {
        loginPage.loginButton.click();
    }*/

    @Then("user should see {string} Message")
    public void user_should_see_Message(String string) {
        Assert.assertEquals(string, loginPage.errorMessage.getText());
    }


}
