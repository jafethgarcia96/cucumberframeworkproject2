package SmartBearSoftwarePages;

import org.openqa.selenium.WebDriver;
import utilities.Driver;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Texts {
    public static WebDriver driver = Driver.getDriver();
    static DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
    static LocalDateTime currentDate = LocalDateTime.now();
    static OrderPage orderPage = new OrderPage(driver);

    //order page
    public static String[] contactInfo = {"Lil Wayne", "2345 McChicken St", "Bronx", "NY", "60180" };
    public static String[] cardInfo= {"4111111111111111", "07/25"};
    public static String[] firstRowData = {contactInfo[0],"FamilyAlbum","2", dateTimeFormatter.format(currentDate),
    contactInfo[1], contactInfo[2], contactInfo[3], contactInfo[4], "Visa", cardInfo[0], cardInfo[1]};
}
