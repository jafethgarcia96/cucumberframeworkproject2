package SmartBearSoftwarePages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class LoginPage {
    public LoginPage(WebDriver driver){
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//input[@class='txt']")
    public List<WebElement> userAndPasswordInputs;

    @FindBy(className = "button")
    public WebElement loginButton;

    @FindBy(className = "error")
    public WebElement errorMessage;
}
