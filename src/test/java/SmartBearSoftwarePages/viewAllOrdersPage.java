package SmartBearSoftwarePages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class viewAllOrdersPage {
    public viewAllOrdersPage(WebDriver driver){
        PageFactory.initElements(driver,this);
    }

    @FindBy(xpath = "//li/a")
    public List<WebElement> menuItems;

    @FindBy(css = ".CheckUncheck>a")
    public List<WebElement> checkUncheck;

    @FindBy(xpath = "//tr/following-sibling::tr/td/input[@type='checkbox']")
    public List<WebElement> checkBoxes;

    @FindBy(xpath = "(//table)[2]//tr[2]/descendant::td")
    public List<WebElement> firstRowTd;


    @FindBy(id = "ctl00_MainContent_btnDelete")
    public WebElement deleteSelectedBttn;


    @FindBy(id = "ctl00_MainContent_orderMessage")
    public WebElement listIsEmptyMssg;


}
