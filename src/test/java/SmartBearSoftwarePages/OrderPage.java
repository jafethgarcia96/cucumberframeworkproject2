package SmartBearSoftwarePages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class OrderPage {

    public OrderPage(WebDriver driver){
        PageFactory.initElements(driver, this);
    }

    @FindBy(tagName = "select")
    public WebElement select;

    @FindBy(xpath = "//input[@style='width:40px;' and not(@maxlength)]")
    public List<WebElement> productInfoInputs;

    @FindBy(xpath = "//input[(@style='width:200px;' and not(@name='ctl00$MainContent$fmwOrder$TextBox6'))]")
    public List<WebElement> addressInfoInputs;

    @FindBy(xpath = "//input[@type='radio']")
    public List<WebElement> paymentRadioBttns;

    @FindBy(xpath = "//input[@name='ctl00$MainContent$fmwOrder$TextBox6' or @maxlength='5']")
    public List<WebElement> paymentInputs;

    @FindBy(className = "btn_light")
    public WebElement processBttn;



}
